using UnityEngine;

[RequireComponent(typeof(MonsterAudioView))]
public class MonsterAudioController : MonoBehaviour
{
    public MonsterAudioView View;

    void Awake()
    {
        View = GetComponent<MonsterAudioView>();
    }

    public void Die()
    {
        InsertClip(View.DyingSound);
        Play();
    }

    public void Play() => View.AudioSource.Play();
    public void InsertClip(AudioClip clip) => View.AudioSource.clip = clip;
}
