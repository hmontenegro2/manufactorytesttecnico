using UnityEngine;

[RequireComponent(typeof(MonsterAudioController))]
[RequireComponent(typeof(MonsterGraphicsController))]
[RequireComponent(typeof(MonsterNetworkController))]
public class MonsterController : MonoBehaviour
{
    public Monster Model {get; private set;}
    public MonsterGraphicsController GraphicsController {get; private set;}
    public MonsterAudioController AudioController {get; private set;}
    public MonsterNetworkController NetworkController {get; private set;}

    void Awake()
    {
        Model = new Monster();

        AudioController = GetComponent<MonsterAudioController>();
        GraphicsController = GetComponent<MonsterGraphicsController>();
        NetworkController = GetComponent<MonsterNetworkController>();

        Model.OnHpChanged += OnHpChanged;
        Model.OnHpChanged += NetworkController.UpdateHealth;
    }

    private void OnHpChanged(int health)
    {
        if (health == 0)
            AudioController.Die();
    }
}
