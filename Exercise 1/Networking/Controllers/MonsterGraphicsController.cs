using UnityEngine;

[RequireComponent(typeof(MonsterGraphicsView))]
public class MonsterGraphicsController : MonoBehaviour
{
    public MonsterGraphicsView View {get; private set;}

    void Awake()
    {
        View = GetComponent<MonsterGraphicsView>();
        AssignSprite(View.Sprite);
    }

    public void AssignSprite(Sprite sprite) => View.Sp.sprite = sprite;
}
