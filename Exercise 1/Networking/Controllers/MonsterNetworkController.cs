using Mirror;

public class MonsterNetworkController : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnHpChangedNet))]
    private int _syncHealth;

    public void UpdateHealth(int health) => _syncHealth = health;

    private void OnHpChangedNet()
    {
        // Networking process
    }
}
