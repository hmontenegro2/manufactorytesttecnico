using UnityEngine;

public class MonsterAudioView : MonoBehaviour
{
    public AudioSource AudioSource;

    public AudioClip DyingSound;

    public MonsterAudioView(AudioSource audioSource)
    {
        this.AudioSource = audioSource;
    }
}
