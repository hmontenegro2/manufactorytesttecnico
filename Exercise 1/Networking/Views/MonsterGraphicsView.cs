using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class MonsterGraphicsView : MonoBehaviour
{
    public SpriteRenderer Sp {get; private set;}
    public Sprite Sprite;

    void Awake()
    {
        Sp = GetComponent<SpriteRenderer>();
    }
}
