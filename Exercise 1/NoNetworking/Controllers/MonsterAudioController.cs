using UnityEngine;

[RequireComponent(typeof(MonsterAudioView))]
public class MonsterAudioController : MonoBehaviour
{
    public MonsterAudioView view;

    void Awake()
    {
        view = GetComponent<MonsterAudioView>();
    }

    public void Die()
    {
        InsertClip(view.dyingSound);
        Play();
    }

    public void Play() => view.audioSource.Play();
    public void InsertClip(AudioClip clip) => view.audioSource.clip = clip;
}
