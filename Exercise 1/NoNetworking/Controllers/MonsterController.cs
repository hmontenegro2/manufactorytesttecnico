using UnityEngine;
using Mirror;

[RequireComponent(typeof(MonsterAudioController))]
[RequireComponent(typeof(MonsterGraphicsController))]
public class MonsterController : NetworkBehaviour
{
    public Monster model {get; private set;}
    public MonsterGraphicsController graphicsController {get; private set;}
    public MonsterAudioController audioController {get; private set;}

    void Awake()
    {
        model = new Monster();
        model.OnHpChanged += OnHpChanged;

        audioController = GetComponent<MonsterAudioController>();
        graphicsController = GetComponent<MonsterGraphicsController>();
    }

    private void OnHpChanged(int health)
    {
        if (health == 0)
            audioController.Die();
    }
}
