using UnityEngine;

[RequireComponent(typeof(MonsterGraphicsView))]
public class MonsterGraphicsController : MonoBehaviour
{
    public MonsterGraphicsView view {get; private set;}

    void Awake()
    {
        view = GetComponent<MonsterGraphicsView>();
        AssignSprite(view.sprite);
    }

    public void AssignSprite(Sprite sprite) => view.sp.sprite = sprite;
}
