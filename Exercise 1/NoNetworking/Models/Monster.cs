using UnityEngine;

public class Monster
{
    public delegate void HpChangeEvent(int newHealth);
    public event HpChangeEvent OnHpChanged;

    public int maxHp;
    public int damage;
    public int id;

    private int _currentHp;

    public void TakeDamage(int damage)
    {
        _currentHp = Mathf.Clamp(_currentHp, 0, _currentHp - damage);
        OnHpChanged?.Invoke(_currentHp);
    }
}
