using UnityEngine;

public class MonsterAudioView : MonoBehaviour
{
    public AudioSource audioSource;

    public AudioClip dyingSound;

    public MonsterAudioView(AudioSource audioSource)
    {
        this.audioSource = audioSource;
    }
}
