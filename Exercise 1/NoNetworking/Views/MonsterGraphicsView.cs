using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class MonsterGraphicsView : MonoBehaviour
{
    public SpriteRenderer sp {get; private set;}
    public Sprite sprite;

    void Awake()
    {
        sp = GetComponent<SpriteRenderer>();
    }
}
