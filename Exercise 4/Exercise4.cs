﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercise4
{
	public class SwarmManager
	{
		private const int MIN_HP = 10;

		// Exercise 4 solution method
		public List<string> GetTankMonstersName(IEnumerable<Swarm> swarms)
		{
			List<string> tankOnes = new List<string>();

			foreach (var swarm in swarms) {
				tankOnes.AddRange(swarm.monsters.Where(monster => monster.HP > MIN_HP).OrderBy(monster => monster.HP).Select(monster => monster.name));
			}

			return tankOnes;
		}
	}

	public class Swarm
	{
		public List<Entity> monsters;
	}

	public class Entity
	{
		public string name;
		public int HP;
	}
}
